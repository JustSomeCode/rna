import sys
from datetime import datetime

#This file filters the SAM file with the filtering criteria below

#===================================================

#Filtering criteria:

#Set the RNA length
RNA_length = 75

#Set the length of each row in the SAM file
SAM_row_len = 12

#lower and upper bounds for alignment scores (AS) (inclusive)
AS_low = -55
AS_up = -15

#lower and upper bounds for number of mismatches (NM) (inclusive)
NM_low = 7
NM_up = 50

#lower bound and upper bounds for length of polyX tail (inclusive), where
#X is given by the letter
stringTail = "A"
polyTail_mismatch = 0
polyTail_low = 2
polyTail_up = 40
polyTail_histogram = [0] * (polyTail_up+1)

#motif to be searched if any ("" denotes none)
motif = ""

#list of chromosomes to be excluded in the search
ignored_Chr = ["chrY","chrM"]



#=====================================================

#read the desired file
datafile = open('all.sam', 'r')

#write to the desired file
output = open('possibleGeneEnds.txt','w')

#=====================================================

#check to make sure AS_low < AS_up
def AScheck():
    if (AS_low > AS_up):
        print "AS_low needs to be <= AS_up"
        sys.exit(0)

#check to make sure 0 < NM_low < NM_up
def NMcheck():
    if (NM_low > NM_up):
        print "NM_low needs to be <= NM_up"
        sys.exit(0)
    if (NM_low < 0):
        print "NM_low needs to be at least 0"
        sys.exit(0)

#Check if polyTail_low < polyTail_mismatch
#and if 0 < polyTail_low 
#and if polyTail_low < polyTail_up < RNA_length
def polyTailCheck():
    if  polyTail_low < polyTail_mismatch:
        print "polyTail_low needs to be greater than polyTail_mismatch"
        sys.exit(0)
    if  polyTail_low < 0:
        print "polyTail_low needs to be at least 0"
        sys.exit(0)
    if  polyTail_low > polyTail_up:
        print "polyTail_low needs to be smaller than polyTail_up"
        sys.exit(0)
    if  polyTail_up > RNA_length:
        print "polyTail_up needs to less than RNA length = " + str(RNA_length)
        sys.exit(0)

#finds alignment scores between AS_low and AS_up
#this method assumes that AS has the form "AS:i:X", where X is an integer
def findAS(data):
    for i in range(len(data)):
        if data[i].startswith('AS:i:'):
            ASvalue=int((data[i])[5:])
            if (AS_low <= ASvalue) and (ASvalue <= AS_up):
                return True

#finds the number of mismatches between NM_low and NM_up
#this method assumes that NM has the form "NM:i:X", where X is an integer
def findNM(data):
    for i in range(len(data)):
        if data[i].startswith('NM:i:'):
            NMvalue=int((data[i])[5:])
            if (NM_low <= NMvalue) and (NMvalue <= NM_up):
                return True

#build a sequence of length thres from the subsequence tailStr
#by concatinating tailStr repeatedly
def buildSeq(tailStr, thres):
    result = ""
    while len(result) < thres:
        result = result + tailStr
    return result[0:thres]

#finds an RNA sequence in a tail of length at least polyTail_low
#returns position of putative cleavage site
#return -1 if doesn't find one...
def findpolyTail(RNA, lets, num):
    seq=buildSeq(lets, polyTail_up)
    for x in range(polyTail_up,polyTail_low-1,-1):
        if RNA[-x-len(lets):-x] != lets: 
            if RNA[-x:] == seq[0:x]:
                polyTail_histogram[x]=polyTail_histogram[x]+1
                return RNA_length-x
            elif polyTail_mismatch > 0: 
                if count(RNA[-x:],seq[0:x],num) == True:
                    polyTail_histogram[x]=polyTail_histogram[x]+1
                    return RNA_length-x
    return -1

#counts the number of mismatches in the tail 
#threshold given by polyTail_mismatch
def count(s,seq,thres):
    mismatch = 0
    for k in range(len(seq)):
        if s[k] != seq[k]:
            mismatch = mismatch + 1
            if mismatch > thres:
                return False
    return True

#finds an RNA sequence with a desired motif
def findMotif(RNAseq, motif):
    if motif == "":
        return True
    elif motif in RNAseq:
        return True

#finds RNA sequences that are from chromosomes NOT in ignored_Chr above
def filterChr(chrom, ignored):
    if not ignored:
        return True
    elif chrom not in ignored:
        return True

#process RNA, prints number of candidates found
#find the RNA sequence that matches the desired AS,NM,
#length of polyA tail, and motif
#this method assumes each row in the .SAM file has at least SAM_row_len entries
#Assumes chrom # is in position 2 and RNA is in position 9
def findEntry():
    candidates=0
    total=0

    for line in datafile:
        data = line.split()

        if len(data) >= SAM_row_len:
            total=total+1
            chrom = data[2]
            RNAseq = data[9]

            if findAS(data)==True and findNM(data)==True \
                and findMotif(RNAseq,motif)==True \
                and filterChr(chrom,ignored_Chr)==True:
                    PCS = findpolyTail(RNAseq, stringTail, polyTail_mismatch)
                    if PCS > 0:
                        candidates=candidates+1

                        #add putative cleavage site field
                        output.write(line.rstrip() + " PCS:" + str(PCS) + '\n')
              
    output.write( "Number of candidates = " + str(candidates) + '\n')
    output.write( "Total = " + str(total) + '\n')
    output.write( "Filtered with: " + '\n')
    output.write( "Minimum alignment score = " + str(AS_low) + '\n')
    output.write( "Maximum alignment score = " + str(AS_up) + '\n')
    output.write( "Minimum number of mismatches = " + str(NM_low) + '\n')
    output.write( "Maximum number of mismatches = " + str(NM_up) + '\n')

    output.write( "Minimum poly" + stringTail + " tail length = " + str(polyTail_low) + '\n')
    output.write( "Maximum poly" + stringTail + " tail length = " + str(polyTail_up) + '\n')
    output.write( "Maximum mismatches allowed in poly" + stringTail + " tail = " + str(polyTail_mismatch) + '\n')

    if motif == "":
        output.write( "motif = NONE" + '\n')
    elif motif != "":
        output.write( "motif = " + str(motif) + '\n' )
    output.write( "Ignored chromosomes: " + str(ignored_Chr))
    output.close()

if __name__ == '__main__':
    print str(datetime.now())
    AScheck()
    NMcheck()
    polyTailCheck()
    findEntry()
    print str(datetime.now())
