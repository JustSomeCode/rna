from datetime import datetime
import numpy as np
import sys
import pdb

#read the desired file
datafile = open('upstream1.txt', 'r')

#write to the desired file
output = open('scored.txt','w')

#===================

#set constants
motif_len = 6
RNA_len = 100

#Read in values from actualGeneEnds file
for line in datafile:
    if "Maximum mismatches allowed in polyA tail" in line:
        mismatches = int(line.split('=')[1])
    elif "Minimum polyA tail length" in line:
        polyTail_low = int(line.split('=')[1])
    elif "Maximum polyA tail length" in line:
        polyTail_up = int(line.split('=')[1])
    elif "Actual gene ends" in line:
        numSeqs = int(line.split('=')[1])

#move pointer back to start
datafile.seek(0)

#Initialize background probabillties
backA = 0.25
backC = 0.25
backG = 0.25
backT = 0.25

#Initialize WMM (background probabilities are in the 0th column)
WMM_0 = np.matrix('1 1 0 1 1 1; \
                   0 0 0 0 0 0; \
                   0 0 0 0 0 0; \
                   0 0 1 0 0 0')

WMM_1 = np.matrix('0.85 0.85 0.05 0.85 0.85 0.85; \
                   0.05 0.05 0.05 0.05 0.05 0.05; \
                   0.05 0.05 0.05 0.05 0.05 0.05; \
                   0.05 0.05 0.85 0.05 0.05 0.05')

#this matrix is generated with model=WMM_1 and actualGeneEnds1.txt
WMM_21 = np.matrix('0.69543145, 0.74748552, 0.19219628, 0.72808974, 0.80652568, 0.80580821; \
                    0.09516431, 0.06300463, 0.10103229, 0.09763086, 0.07934959, 0.05371417; \
                    0.08841871, 0.0839874 , 0.15909266, 0.12449312, 0.06408714, 0.08469671; \
                    0.07684479, 0.06651567, 0.54767876, 0.04978629, 0.05003759, 0.05578091')

#this matrix is generated with model=WMM_1 and actualGeneEnds2.txt
WMM_22 = np.matrix('0.62003556, 0.66174017, 0.14953175, 0.65536538, 0.69703477, 0.70746647; \
                    0.12149238, 0.09226635, 0.11980345, 0.13277198, 0.12166903, 0.09613825; \
                    0.11578961, 0.11187347, 0.18661242, 0.14692177, 0.109225, 0.11003048; \
                    0.07665399, 0.079572, 0.54372889, 0.06476655, 0.06960006, 0.08553403')

#this matrix is generated with model=WMM_1 and upstream1.txt
WMM_23 = np.matrix('0.618,0.684,0.360,0.595,0.913,0.717; \
                    0.054,0.005,0.097,0.077,0.013,0.017; \
                    0.236,0.260,0.257,0.326,0.059,0.250; \
                    0.092,0.051,0.286,0.002,0.015,0.016')

#test model
WMM_3 = np.matrix('0.97 0.97 0.97 0.97 0.97 0.97; \
                   0.01 0.01 0.01 0.01 0.01 0.01; \
                   0.01 0.01 0.01 0.01 0.01 0.01; \
                   0.01 0.01 0.01 0.01 0.01 0.01')


#denote model as a WMM
model = WMM_1

#initialize matrices
Z = np.zeros(shape=(numSeqs,RNA_len-motif_len+1)) #dimensions: (#seq) x (L-W+1)
P = model.copy() #dimensions: 4 x motif_len

#tolerance for EM algorithm
relEntTol=10**(-2)

#================================================================

def checkColSum(model):
    #check if the sum of each column of the model matrix is equal to 1
    colSums = np.sum(model, axis=0)
    for x in range(colSums.shape[1]):
        if colSums[0,x] != 1:
            print "Column " + str(x) + " of the model does not sum to 1!"
            sys.exit(0)


#Turn WMM into LLR matrix
#0th row: frequency of A
#1st row: frequency of C
#2nd row: frequency of G
#3rd row: frequency of T
def createLLR(matrix):
    rows=matrix.shape[0]
    cols=matrix.shape[1]
    LLR = np.zeros(shape=(rows,cols))
    for j in range(rows):
        if j == 0:
            LLR[j]=matrix[j]/backA
        elif j == 1:
            LLR[j]=matrix[j]/backC
        elif j == 2:
            LLR[j]=matrix[j]/backG
        elif j == 3:
            LLR[j]=matrix[j]/backT
    LLR = np.log2(LLR)
    return LLR

#Given a sequence, compute the LLR score of all subsequences 
def LLRscore(seq, matrix):
    scores=np.zeros(shape=(1,len(seq)-motif_len+1))
    for k in range(len(seq)-motif_len+1):
        subseq = seq[k:k+motif_len]
        value=0
        for m in range(len(subseq)):
            if subseq[m] == 'A':
                value = value + matrix[0,m]     
            elif subseq[m] == 'C':
                value = value + matrix[1,m]  
            elif subseq[m] == 'G':
                value = value + matrix[2,m]  
            elif subseq[m] == 'T':
                value = value + matrix[3,m]
            else:
                value = value + float("-inf")
        scores[0,k]=value
    return scores

    

#find highest LLR score, return closest one to the end if there is a tie**
#find max value of numpy array, start searching from the end to find index
#that equals the max
def maxIndex(array):
    thres = 0
    maxValue=0
    index = -1
    for j in range( array.shape[1]-1, -1, -1 ):
        value = array[0,j]
        #score is greater than 0 
        if value > maxValue: 
            maxValue=value
            index=j+1

    #did not find an entry with distance greater than motif len
    if index == -1:
        return -1
    
    return index

#return the index of the putative cleavage site
def findPCS(line):
    data = line.split()
    for i in range(len(data)):
        if data[i].startswith('PCS:'):
            return int((data[i])[4:])

#Given a file of sequences, compute LLR score of every sequence
def LLR():
    LLR_matrix = createLLR(model)
    count = 0 #count number of entries with LLR score > 0
    distance=0
    total=0
    histogram = [0] * (RNA_len + 1)
    countD = 0 
    for line in datafile:
        data = line.split()
        if len(data) == 3 and "ERR" in data[0]:

            RNAseq = data[1].upper()
            scores = LLRscore(RNAseq, LLR_matrix)

            maxInd = maxIndex(scores) 
            if maxInd > -1:
                output.write(RNAseq)
                output.write('\n')
                scores.tofile(output, sep=" ", format="%s")
                output.write('\n')
                distance = len(RNAseq) - maxInd + 1
                histogram[distance] = histogram[distance]+1
                total = total + distance

                output.write(str(distance))
                output.write('\n')

                count = count+1
            
            else:
                 histogram[0]=histogram[0]+1
        elif len(data) > 0:
            output.write(line)

    #move pointer back to start
    datafile.seek(0)
            
    #compute the average distance
    if count > 0:
        total = total/count
    else:
        total =  float("inf")
    
    output.write('\n')
    output.write("Number of entries with LLR score > 0: " + str(count) + '\n')
    output.write("Average distance = " + str(total) + '\n')
    print histogram
    output.close()


#given a seq, compute the entries of the Z matrix (E-step) with the p matrix
#(try on small example first!!!)
#this method assumes uniform background distribution!!!
def computeZ(seq, start):
    value = (0.25)**(RNA_len-motif_len) 
    subseq = seq[start:start+motif_len]
    for j in range(motif_len):
        if subseq[j] == 'A':
            value = value * P[0,j]     
        elif subseq[j] == 'C':
            value = value * P[1,j]  
        elif subseq[j] == 'G':
            value = value * P[2,j]  
        elif subseq[j] == 'T':
            value = value * P[3,j]
    return value

#create the initial Z matrix (E-step) (OK tested)
def createZ():
    i=0
    for line in datafile:
        data = line.split()
        if len(data) == 3 and "ERR" in data[0]:
            RNAseq = data[1].upper() 

            for k in range(RNA_len-motif_len+1):
                Z[i][k] = computeZ(RNAseq, k)

            #normalize so the sum of each row is 1
            if np.sum(Z[i]) != 0:
                factor = 1/np.sum(Z[i])
                Z[i]=factor*Z[i]
    
            i=i+1
            
    #move pointer back to start
    datafile.seek(0)

#given a Z matrix, compute the p matrix (M-step)
def computeP(letter, position, sequence, seqInd):
    numerator = 0
    for x in range(RNA_len - motif_len + 1):
        subseq = sequence[x:x+motif_len]
        if subseq[position] == letter:
            numerator = numerator + Z[seqInd][x] #(just need start of motif!)
            
    return numerator

#create P matrix: 
def createP():
    ind = 0
    pVal = 0
    bases = ['A', 'C', 'G', 'T']
    for x in range(len(bases)): #char loop
        base = bases[x]
        for j in range(motif_len): #position loop
            basePos = j
            for line in datafile: #seqs loop
                data = line.split()
                if len(data) == 3 and "ERR" in data[0]:
                    RNAseq = data[1].upper()
                    pVal = pVal + computeP(base, basePos, RNAseq, ind)
                    ind = ind + 1

            P[x,j] = float(pVal/numSeqs) #normalized Z matrix so every row sums to 1
            pVal = 0
            ind = 0
            
            
            #move pointer back to start
            datafile.seek(0)
            
    #move pointer back to start
    datafile.seek(0)


#compute relative entropy given a P matrix (OK, tested)
def relEnt(pMatrix):
    rows=pMatrix.shape[0]
    cols=pMatrix.shape[1]

    relEnt = np.zeros(shape=(1,cols))

    for j in range(cols):
        for i in range(rows):
            if pMatrix[i,j] != 0:
                if i == 0: #corresponds to A
                    relEnt[0,j] = relEnt[0,j] + pMatrix[i,j] * np.log2(pMatrix[i,j]/backA)
                elif i == 1: #corresponds to C
                    relEnt[0,j] = relEnt[0,j] + pMatrix[i,j] * np.log2(pMatrix[i,j]/backC)
                elif i == 2: #corresponds to G
                    relEnt[0,j] = relEnt[0,j] + pMatrix[i,j] * np.log2(pMatrix[i,j]/backG)
                elif i == 3: #corresponds to T
                    relEnt[0,j] = relEnt[0,j] + pMatrix[i,j] * np.log2(pMatrix[i,j]/backT)

    return np.sum(relEnt)
    
    
#implement the EM algorithm
def EM():
    t=0
    prevEnt = -1 #entropy is >=0
    currentEnt = relEnt(model)
    while abs(currentEnt - prevEnt) > relEntTol:
        print currentEnt
        t=t+1
        prevEnt=currentEnt
        createZ()
        createP()
        currentEnt = relEnt(P)
    print currentEnt
    print "Number of Iterations: " + str(t)
    print P
    print np.sum(P, axis=0)


if __name__ == '__main__':
    checkColSum(model)
    LLR()
    print str(datetime.now())
    EM()
    print str(datetime.now()) 











    
