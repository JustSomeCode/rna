from datetime import datetime
import MySQLdb as mdb
import sys
import os

# Open database connection
db = mdb.connect(host="localhost", 
                     user="root", 
                      passwd="REMOVED", # your password (REMOVED!)
                      db="genome") # name of the data base

# prepare a cursor object using cursor() method
cursor = db.cursor()

#=======================================
#get the name of all files in the same directory as pySQL.py
def getFileNames():
    
    #store all file names in the same directory as index.py
    fileNames = []

    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if f.startswith("Indexed"):
            fileNames.append(f)
            
    return fileNames

#============================================================

def createTable(name):
   # Drop table if it already exist using execute() method.
   cursor.execute("DROP TABLE IF EXISTS " + name)

   # Create table as per requirement
   sql = """CREATE TABLE """ + name + """(
            chr varchar(255) NOT NULL,
            start varchar(255) NOT NULL,
            DNA varchar(255) )"""

   cursor.execute(sql)

#=============================================================

#load genome sequences into tables
def storeDNA():

    files = getFileNames()

    for f in files:
       
        name=f[:-3]
        createTable(name)

        print name
        
        cursor.execute("LOAD DATA INFILE " + "'" + f + "'" + " INTO TABLE " + name \
                       + " FIELDS TERMINATED BY ' ' " + " LINES TERMINATED BY '\r\n'")

    # disconnect from server
    db.close()

   
print str(datetime.now()) 
storeDNA()
print str(datetime.now()) 
