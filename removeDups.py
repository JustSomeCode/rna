from datetime import datetime

#read the desired file
datafile = open('rawGenes', 'r')

#write to the desired file
output = open('filteredGenes.txt','w')

#remove duplicate entries in raw genes
def removeDups():
    total=0
    noDups=0
    prevChrm=""
    prevGeneEnd=""

    for line in datafile:
        data = line.split()
        total=total+1

        #assumes chrm# is in the first position and geneEnd position is in the
        #third position
        chrm = data[0]
        geneEnd = data[2]

        if chrm == prevChrm and geneEnd == prevGeneEnd:
            continue
        else:
            output.write(chrm + " " + geneEnd + '\n')
            prevChrm = chrm
            prevGeneEnd = geneEnd
            noDups = noDups + 1

    output.write('\n')
    output.write('\n' + "Total Entries = " + str(total)+ '\n' )
    output.write("Total w/o duplicates = " + str(noDups) )
    output.close()

print str(datetime.now())
removeDups()
print str(datetime.now())
