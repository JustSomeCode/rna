import os

#length of each line in each chr* file (there are 50 chars and 1 blank space)
thres = 50

#get the name of all files in the same directory as index.py
def getFileNames():
    
    #store all file names in the same directory as index.py
    fileNames = []

    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if ".py" not in f:
            fileNames.append(f)
            
    return fileNames

#open each chr* file, where * is the chr name, index each line
#and output into another file
def indexFiles():
    names = getFileNames()
    for name in names:
    
        datafile = open(name, 'r')
        indexed = open('Indexed'+name,'w')
        ind = 1
        chrstr = name[:-3]

        for line in datafile:
            if len(line) > thres:
                indexed.write(chrstr + " " + str(ind) + " " + line)
                ind = ind+50
        indexed.close()


indexFiles()
