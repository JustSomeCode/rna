from datetime import datetime
import MySQLdb as mdb
import sys
import os

#This file pulls the appropriate sequences from the genome stored in a MySQL
#database that correspond to the reads in sortedGenes.txt

#read desired file
sortedG = open('sortedGenes.txt', 'r')

#write to desired file
pulled = open('upstream.txt', 'w')

sortedGenesLineLength = 5
upstream = 100
RNA_len = 75
genomeLineLength = 50
readPrefix="ERR"

# Open database connection
db = mdb.connect(host="localhost", # your host, usually localhost
                     user="root", # your username
                      passwd="REMOVED", # REMOVED
                      db="genome") # name of the data base

# prepare a cursor object using cursor() method
cursor = db.cursor()

#trims the DNA so it of the correct length
def trim(DNA, cutoff):
    if cutoff > 0:
        DNA = DNA[:-cutoff]       
    elif cutoff < 0:
        print "Cutoff is less than 0!"
        sys.exit(0)

    DNA=DNA[-upstream:]
    return DNA
    
#pull upstream sequence from the putative cleavage site PCS
def getUpstream():
    
    prevChr=""
    prevPCS=""
    prevReadStart=""
    prevGenomeSeq=""

    for line in sortedG:

        data = line.split()
        if len(data) == sortedGenesLineLength and readPrefix in data[0]:

            readID = data[0]
            chrmNum = data[1]
            readStart = data[2]
            PCS = (data[4])[4:]

            if prevChr==chrmNum and prevPCS == PCS and prevReadStart == readStart:
                pulled.write(readID + " " + prevGenomeSeq + " PCS:" + PCS + '\n')
                print prevGenomeSeq

            else:
                End = int(readStart) + int(PCS) - 1
                Start = End - upstream + 1

                pullEnd = End

                #1+(genomeLineLength)n
                if (Start - 1)%genomeLineLength != 0:
                    temp = (Start-1)/genomeLineLength
                    Start = temp*genomeLineLength + 1
                if (End - 1)%genomeLineLength != 0:
                    temp = (pullEnd-1)/genomeLineLength
                    End = temp*genomeLineLength + 1 #(MySQL BETWEEN is inclusive)

                # Prepare SQL query to FETCH a record from the database.
                sql = "SELECT DNA FROM indexed" + chrmNum + \
                       " WHERE START BETWEEN " + str(Start) + " AND " + str(End)

                print sql

                try:
                   # Execute the SQL command
                   cursor.execute(sql)
                except:
                   print "Error: unable to get data"

                #concatenate the sequences from different rows
                results = cursor.fetchall()
                genomeSeq = ""
                for row in results:
                    genomeSeq = genomeSeq + row[0]
                    
                #trim genomeSEQ!!!
                trimEnd = End + genomeLineLength-1 - pullEnd 
                genomeSeq = trim(genomeSeq, trimEnd)
                
                #reassign...
                prevChr=chrmNum
                prevPCS = PCS
                prevReadStart = readStart
                prevGenomeSeq = genomeSeq
                    
                pulled.write(readID + " " + genomeSeq + " PCS:" + PCS + '\n')
                print genomeSeq

                if len(genomeSeq) != upstream:
                    print "Length of genome sequence is not equal to " + str(upstream)
                    sys.exit(0)
        else:
            pulled.write(line)
            
    pulled.close()

    # disconnect from server
    db.close()

print str(datetime.now())
getUpstream()
print str(datetime.now())
        
