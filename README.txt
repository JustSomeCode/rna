Download:
all.sam
rawGenes: Tools --> table browser
	-clade: mammal; genome: human; assembly: Feb 2009
	-group:genes and gene predictions
	-track UCSC genes
	-region: genome
	-output format: BED-browser extensible data
chr*.fa (genome sequences): http://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/ 
	-chromFa.tar.gz 


**Order to run**:
searchSAM.py: all.sam ---> possibleGeneEnds.txt
removeDups.py: rawGenes ---> filteredGenes.txt
findGeneEnds.py: filteredGenes.txt, possibleGeneEnds.txt ---> actualGeneEnds.txt ---> sortedGenes.txt

MySQL stuff
index.py: chr*.fa ---> indexedchr*.fa; where * denotes the name of the chr
pySQL.py: indexedchr*.fa ---> genome database
pull.py: sortedGenes.txt --> upstream.txt

EM.py: upstream.txt ---> scored.txt

