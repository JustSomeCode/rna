import sys
from datetime import datetime
from operator import itemgetter

#read the desired files
filtered = open('filteredGenes.txt', 'r')
possible = open('possibleGeneEnds.txt', 'r')


#write to the desired file
output = open('actualGeneEnds.txt','w')

RNA_len = 75
thres=12

#Global variables
allFilt = []
sortedFilt = []

#position: can be "start"/"end"
#if processing seqs with T's at the front: use "start"
#if processing seqs with A's at the end: use "end"
position = "end"



#class containing chromosome number and gene end
class Entry:
    def __init__(self, chrmNum=None, geneStart=0, geneEnd=0):
        self.chrmNum = chrmNum
        self.geneStart = geneStart
        self.geneEnd = geneEnd
        self.triple = (chrmNum, int(geneStart), int(geneEnd))

    def __getitem__(self, key):
        return self.triple[key]

#class containing chrm num and read start
class Read:
    def __init__(self,readID = None, chrmNum=None, readStart=0, seq = None, PCS = None):
        self.readID = readID
        self.chrmNum = chrmNum
        self.readStart = readStart
        self.seq = seq
        self.PCS = PCS
        self.group= (readID, chrmNum, int(readStart), seq, PCS)

    def __getitem__(self, key):
        return self.group[key]

#displays the chrmNum given a list
def printChrm(list0):
    prev = ""
    for x in range(len(list0)):
        chrm = list0[x].chrmNum
        if chrm  != prev:
            print chrm 
            prev = chrm


#check if list1 is filtered by chrm number and gene *end*/*start* position
#will print if the order is not followed
def checkFilt(list1, pos):
    for x in range(len(list1)):
        if x+1 < len(list1):
            if list1[x].chrmNum == list1[x+1].chrmNum:
                if pos == "end":
                    if int(list1[x].geneEnd) > int(list1[x+1].geneEnd):
                        print "not sorted"
                elif pos == "start":
                    if int(list1[x].geneStart) > int(list1[x+1].geneStart):
                        print "not sorted"

#get all chrm# and gene ends/start from filtered 
def getFiltered(pos):
    for line in filtered:
        data = line.split()
        if len(data) == 2: #this number is important
            if pos == "end":
                allFilt.append( Entry(chrmNum=data[0], geneStart =  -1, geneEnd=data[1]) )
            elif pos == "start":
                allFilt.append( Entry(chrmNum=data[0], geneStart=data[1], geneEnd = -1) )

#sort all entries in allFilt and place into new list called sortedFilt
def createSorted(pos):
    getFiltered(pos)
    if pos == "start":
        temp = sorted(allFilt, key=itemgetter(0,1)) #position important!
    elif pos == "end":
        temp = sorted(allFilt, key=itemgetter(0,2)) #position important!

    #add elements to new list called sortedFilt
    for k in range(len(temp)):
        sortedFilt.append(temp[k])



#binary search
#returns a boolean 
def binarySearch(aList, start, end, readChrm, readPos):
    while start <= end:
        mid = (start + end)/2
        if str(readChrm) < str(aList[mid].chrmNum):
            end=mid-1
        elif str(readChrm) > str(aList[mid].chrmNum):
            start=mid+1
        else: #readChrm == aList[mid].chrmNum
            if position == "end":
                if int(aList[mid].geneEnd) <= readPos <= int(aList[mid].geneEnd) + (RNA_len - 1):
                    return True
                elif readPos < int(aList[mid].geneEnd):
                    end=mid-1
                elif readPos > int(aList[mid].geneEnd) + (RNA_len - 1):
                    start = mid+1
            elif position == "start":
                if int(aList[mid].geneStart)-(RNA_len - 1) <= readPos <= int(aList[mid].geneStart):
                    return True
                elif readPos < int(aList[mid].geneStart)-(RNA_len - 1):
                    end=mid-1
                elif readPos > int(aList[mid].geneStart):
                    start = mid+1
    return False

            
#Uses binary search and checks interval. 
def getEnds(order):
    actual = 0
    for line in possible:    
        data = line.split()
        
        if len(data) >= thres:
            chrmNum=data[2]
            if order == "end":          
                endPos = int(data[3])+(RNA_len - 1) #end of the read

                if binarySearch(sortedFilt, 0, len(sortedFilt)-1, chrmNum, endPos) == True:
                    output.write(line)
                    actual = actual + 1

            elif order == "start":
                startPos = int(data[3]) #start of the read

                if binarySearch(sortedFilt, 0, len(sortedFilt)-1, chrmNum, startPos) == True:
                    output.write(line)
                    actual = actual + 1

        else:        
            output.write(line)
        
    output.write('\n' + "Actual gene ends = " + str(actual))
    output.close()

#sort actual gene ends and group chr num and start position by overlaps
def getOverlap():
    readInfo = []
    actual = open('actualGeneEnds1.txt','r')
    out = open('sortedGenes.txt', 'w')
    temp = []
    for line in actual:
        data = line.split()
        if len(data)>=thres:
            temp.append( Read(readID= data[0],chrmNum=data[2], readStart = data[3], seq = data[9], PCS = data[-1]))
        else:
            readInfo.append(line)
                
    sortedtemp = sorted(temp, key=itemgetter(1,2,0))

    prevStart=float('-inf')
    curStart=0
    prevChr = ""
    curChr = ""
    for ele in sortedtemp:
        curStart=int(ele.readStart)
        curChr = ele.chrmNum
        if abs(curStart - prevStart) > RNA_len or prevChr != curChr:
            out.write('\n')
        out.write(ele.readID + " " + ele.chrmNum + " " + ele.readStart  + " " + ele.seq + " " + ele.PCS + '\n')
        prevStart=curStart
        prevChr = ele.chrmNum

    out.write('\n')
    for info in readInfo:
        out.write(info)




createSorted(position) #creates sortedFilt
checkFilt(sortedFilt, position) #checks if sortedFilt is indeed sorted

print str(datetime.now())   
getEnds(position)
print str(datetime.now())

getOverlap()
